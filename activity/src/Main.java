import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = 0, ans = 1, counter = 1;
        System.out.print("Input an integer whose factorial will be computed: ");

        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }

        while (counter <= num) {
            ans *= counter;
            counter++;
        }

        if (num < 0) {
            System.out.println("Factorial of a negative number does not exist..");
        } else {
            System.out.println("The factorial of " + num + " is " + ans + "!");
        }
    }
}